/**
 * File: ask_freedb.cpp
 * Author: Thomas Juerges (thomas.juerges@astro.ruhr-uni-bochum.de)
 * License: GPL
 *
 * $Id$
 *
 */


#include <iostream>
#include <iomanip>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <cstdarg>
#include <cerrno>

#include <QCoreApplication>
#include <QString>
#include <QStringList>
#include <QFile>

#include <fcntl.h>
#include <netdb.h>
#include <unistd.h>
#include <cstring>
#include <sys/ioctl.h>
#include <sys/types.h>

#include <linux/cdrom.h>

#include <ask_freedb.h>


QString answer;
bool print_debug = FALSE;


int main(int argc, char* argv[])
{
	const QString version(VERSION), compilation(COMPILATION_DATE);

    const int port(888);
    QString drive, server;

    QCoreApplication a(argc, argv);
    std::cout << "\n"
        << argv[0]
        << ": (c) 2002 by Thomas Juerges\n"
            "E-Mail: thomas.juerges@astro.ruhr-uni-bochum.de\n"
            "License: GPL\n"
            "Version: "
        << version.toStdString()
        << " ("
        << compilation.toStdString()
        << ")\n\n";

    if(argc < 3)
    {
        std::cout << "Please add server name and path to CD-ROM drive:\n"
            << argv[0]
            << " [server] [devicepath] debug\n";
        return -ENOENT;
    }
    else
    {
        const QString print_debug_str("debug");
        if(argc == 4 && (print_debug_str == argv[3]))
        {
            print_debug = true;
        }

        server = argv[1];
        if(print_debug)
        {
            std::cout << "Server to be asked on port cddbp (888): "
                << server.toStdString()
                << "\n";
        }

        const QString dummy("/dev/");
        if(QString::compare(dummy, argv[2]) > 0)
        {
            std::cout << "Please add the correct path for the CD-ROM drive"
                << "containing the audio CD:\n"
                << argv[0]
                << "server /dev/DEVICE\n";
            return -ENOENT;
        }
        else
        {
            drive = argv[2];
        }
    }

    audio_cd* cd(new audio_cd(drive));

    if(print_debug)
    {
        std::cout << "\nDisc ID="
            << std::setw(8)
            << std::setbase(16)
            << cd->get_discid()
            << "\nNumber of tracks = "
            << std::setbase(10)
            << cd->get_tracks()
            << "\n";
    }

    Socket* socket(new Socket);
    socket->connectToHost(server, port);
    socket->waitForConnected(10000);
    if(socket->state() != QAbstractSocket::ConnectedState)
    {
        std::cout << "Could not connect.\n";
        exit(-1);
    }

    if(print_debug)
    {
        std::cout << answer.toStdString();
    }

    QString cmd = "cddb hello ";
    cmd.append(getenv("USER"));
    cmd += " ";
    cmd.append(getenv("HOSTNAME"));
    cmd += " ask_freedb 0.1\n\0";

    if(print_debug)
    {
        std::cout << "Handshake with FreeDB: "
            << cmd.toStdString()
            << "\n";
    }

    socket->write(cmd.toLatin1(), cmd.length());
    socket->flush();
    socket->waitForReadyRead(-1);

    if(print_debug)
    {
        std::cout << answer.toStdString();
    }

    cmd = "proto 5\n\0";
    if(print_debug)
    {
        std::cout << "Setting protocol level: "
            << cmd.toStdString()
            << "\n";
    }

    socket->write(cmd.toLatin1(), cmd.length());
    socket->flush();
    socket->waitForReadyRead(-1);

    if(print_debug)
    {
        std::cout << answer.toStdString()
            << "Disc ID = "
            << cd->get_discid()
            << "\nTracks: "
            << cd->get_tracks()
            << "\n";
    }

    cmd.sprintf("cddb query %lx %d", cd->get_discid(), cd->get_tracks());

    QString dummy;
    for(unsigned int i(0U); i < cd->get_tracks(); ++i)
    {
        dummy.sprintf(" %d", cd->get_frames(i));
        cmd.append(dummy);
    }

    cmd += " ";
    dummy.setNum(cd->get_frames(cd->get_tracks()) / 75);
    cmd += dummy;
    cmd += "\n\0";

    if(print_debug)
    {
        std::cout << "FreeDB query: "
            << cmd.toStdString()
            << "\n";
    }

    socket->write(cmd.toLatin1(), cmd.length());
    socket->flush();
    socket->waitForReadyRead(-1);

    if(print_debug)
    {
        std::cout << answer.toStdString();
    }

    QString category, id, error(answer);
    unsigned short ret(0U);

    ret = answer.left(3).toUShort();
    answer.remove(0, 4);

    int idx(-1), i(1), number(-1);

    int hours(0), minutes(0);
    float seconds(0.);

    bool flag(false);

    QStringList list, titles;
    QString disc_title, title, str;

    switch(ret)
    {
        case 211:
        {
            list = answer.split("\n");
            std::cout << "Found more than one unique occurence.\n"
                "Select the appropriate entry number.\n";

            i = 1;
            list.removeFirst();
            list.removeLast();
            for(QStringList::Iterator it = list.begin();
                it != list.end(); ++it, ++i)
            {
                std::cout << i
                    << ".\t"
                    << (*it).simplified().toStdString()
                    << "\n";
            }

            do
            {
                std::cout << "\nEnter number: ";
                std::cin >> number;
                std::cout << "\n";
            }
            while((number <= 0) || (number >= i));

            answer = list[number - 1];
            ret = 200;
            flag = true;
        }
        // Fall through to 210:

        case 210:
        {
            if(!flag)
            {
                list = answer.split("\n");
                list.removeFirst();
                answer = list[0];
                ret = 200;
            }
        }
        // Fall through to 200:

        case 200:
        {
            list = answer.split(" ");
            category = list[0];
            id = list[1];
            cmd = "cddb read ";
            cmd += category;
            cmd += " ";
            cmd += id;
            cmd += "\n\0";

            if(print_debug)
            {
                std::cout << "Asking for title names: "
                    << cmd.toStdString()
                    << "\n";
            }

            socket->write(cmd.toLatin1(), cmd.length());
            socket->flush();
            socket->waitForReadyRead(-1);

            if(print_debug)
            {
                std::cout << answer.toStdString();
            }

            list = answer.split("\n");
            if(print_debug)
            {
                for(QStringList::Iterator it(list.begin());
                    it != list.end(); ++it)
                {
                    std::cout << (*it).simplified().toStdString()
                        << "\n";
                }
            }

            //titles = list.grep("TITLE");
            int titlesIndex(-1);
            while((titlesIndex = list.indexOf("TITLE", titlesIndex + 1)) != -1)
            {
                titles << list.at(titlesIndex);
            };

            disc_title = *(titles.begin());
            disc_title = disc_title.right(disc_title.length() - 7);
            titles.removeFirst();
            std::cout << "Disc title: "
                << disc_title.simplified().toStdString()
                << "\n";

            cd->get_total_time(hours, minutes, seconds);
            str.sprintf("Total playing time [hh:mm:ss]: %02d:%02d:%02d", hours,
                minutes, static_cast< int >(seconds));

            std::cout << str.simplified().toStdString()
                << "\n\nAlbum tracks:\n";

            i = 1;
            for(QStringList::Iterator it(titles.begin());
                it != titles.end(); ++it, ++i)
            {
                cd->get_track_time(i, hours, minutes, seconds);
                title = (*it);
                idx = title.indexOf('=');
                title = title.right(title.length() - (idx + 1));
                str.sprintf("%02d. %s %02d:%02d:%02d", i,
                    title.toStdString().c_str(), hours, minutes,
                    static_cast< int >(seconds));
                std::cout << str.simplified().toStdString()
                    << "\n";
            }

            std::cout << "\n";
        }
        break;

        default:
        {
            std::cout << "\nAn error occured. Please run the program again"
                "\nwith option debug!\n"
                "The freedb database returned:\n"
                << error.simplified().toStdString()
                << "\n\n";
        }
        break;
    }

    delete cd;
    delete socket;

    exit(0);
}
