#
# $Id$
#


TEMPLATE = app
TARGET = ask_freedb

LANGUAGE = C++

CONFIG += qt rtti stl warn_on debug
QT += network
QT -= gui

QMAKE_CXXFLAGS_RELEASE += $$system(cflags.sh QMAKE_RELEASE) \
    -DCOMPILATION_DATE=\\\"`date --iso-8601=seconds`\\\" \
    -DVERSION=\\\"`./compilation_run`\\\"
QMAKE_CXXFLAGS_DEBUG += $$system(cflags.sh QMAKE_DEBUG) \
    -DCOMPILATION_DATE=\\\"`date --iso-8601=seconds`\\\" \
    -DVERSION=\\\"`./compilation_run`\\\"

HEADERS += ask_freedb.h

SOURCES += ask_freedb.cpp

