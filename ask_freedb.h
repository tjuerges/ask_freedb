#ifndef __cplusplus
#error This is a C++ include file and cannot be used from plain C.
#endif
#ifndef ask_freedb_h
#define ask_freedb_h
/**
 * File: ask_freedb.hxx
 * Author: Thomas Juerges (thomas.juerges@astro.ruhr-uni-bochum.de)
 * Date: 2002/09/23
 * License: GPL
 *
 * $Id$
 *
 */


#include <iostream>
#include <iomanip>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <cstdarg>
#include <cerrno>

#include <QString>
#include <QStringList>
#include <QFile>
#include <QtNetwork>

#include <fcntl.h>
#include <netdb.h>
#include <unistd.h>
#include <cstring>
#include <sys/ioctl.h>
#include <sys/types.h>

#include <linux/cdrom.h>
#include <iostream>

extern QString answer;
extern bool print_debug;

//We use "only" 100 titles of any audio CD:
#define MAX_TOC_ENTRIES 100


class Socket: public QTcpSocket
{
    Q_OBJECT
    public:
    Socket():
        QTcpSocket()
    {
        connect(this, SIGNAL(hostFound()), SLOT(sock_host_found()));
        connect(this, SIGNAL(connected()), SLOT(sock_connected()));
        connect(this, SIGNAL(readyRead()), SLOT(sock_incoming_data()));
        connect(this,SIGNAL(bytesWritten(int)),SLOT(sock_bytes_written(int)));
        connect(this, SIGNAL(connectionClosed()), SLOT(
            sock_connection_terminated()));
        connect(this,SIGNAL(error(int)),SLOT(sock_error(int)));
        connect(this, SIGNAL(delayedCloseFinished()),
            SLOT(sock_delete_socket()));
    };
    ~Socket()
    {
    };

    private slots:
    void sock_host_found()
    {
        if(print_debug)
        {
            std::cout<<"Socket: host found.\n";
        }
    };

    void sock_connected()
    {
        if(print_debug)
        {
            std::cout << "Socket: connection established.\n";
        }
    };

    void sock_connection_terminated()
    {
        if(print_debug)
        {
            std::cout << "Socket: connection terminated.\n";
        }
    };

    void sock_error(int num)
    {
        if(print_debug)
        {
            std::cout << "Socket: error #" << num << " occured.\n";
        }
    };

    void sock_bytes_written(int num)
    {
        if(print_debug)
        {
            std::cout << "Socket: " << num << " bytes written.\n";
        }
    };

    void sock_incoming_data()
    {
        if(print_debug)
        {
            std::cout << "Socket: Incoming data.\n";
        }

        QTcpSocket* sock(static_cast< QTcpSocket* >(sender()));
        QString line;
        char c(0U);

        do
        {
            if(sock->getChar(&c) == true)
            {
                line += c;
            }
        }
        while(sock->bytesAvailable() != 0);
        answer = line;
    };

    void sock_delete_socket()
    {
        QTcpSocket* sock(static_cast< QTcpSocket* >(sender()));
        delete sock;
        sock = 0;

        if(print_debug)
        {
            std::cout << "Socket: socket deleted.\n";
        }
    };
};

class toc
{
    public:
    toc():
        min(0), sec(0), frames(0)
    {
    };

    ~toc()
    {
    };

    void copy(const toc& from)
    {
        if(&from != this)
        {
            min = from.min;
            sec = from.sec;
            frames = from.frames;
        }
    };

    toc& operator=(const toc& from)
    {
        if(&from == this)
        {
            return (*this);
        }
        else
        {
            min = from.min;
            sec = from.sec;
            frames = from.frames;
        }

        return (*this);
    };

    void set(int minutes, int seconds, int fframes)
    {
        if(minutes > 0)
        {
            min = minutes;
        }

        if(seconds > 0)
        {
            sec = seconds;
        }

        if(fframes > 0)
        {
            frames = fframes;
        }
    };

    void clear()
    {
        set(0, 0, 0);
    };

    void set_min(int minutes)
    {
        if(minutes > 0)
        {
            min = minutes;
        }
    };

    void set_sec(int seconds)
    {
        if(seconds > 0)
        {
            sec = seconds;
        }
    };

    void set_frames(int fframes)
    {
        if(fframes > 0)
        {
            frames = fframes;
        }
    };

    int get_min()
    {
        return (min);
    };

    int get_sec()
    {
        return (sec);
    };

    int get_frames()
    {
        return (frames);
    };


    private:
    int min;
    int sec;
    int frames;
};


class audio_cd
{
    public:
    audio_cd(QString audio_cd_device):
        drive(audio_cd_device),
        file(drive),
        drive_file(0),
        tracks(0),
        discid(0)
    {
        open_drive();
        tracks = read_toc();
        discid = cddb_discid();
    };

    ~audio_cd()
    {
        if(drive_file != 0)
            close(drive_file);
    };

    unsigned long get_discid()
    {
        return (discid);
    };

    unsigned int get_tracks()
    {
        return (tracks);
    };

    int get_frames(int track)
    {
        if((track <= (int) get_tracks()) && (track <= MAX_TOC_ENTRIES)
            && (track >= 0))
            return (cdtoc[track].get_frames());
        else
            return ( -1);
    };

    void get_track_time(unsigned int track, int& hours, int& minutes,
        float& seconds)
    {
        if(track == 0)
        {
            seconds = (float) cdtoc[track].get_frames() / 75.;
        }
        else
        {
            seconds = (float) (cdtoc[track].get_frames()
                - cdtoc[track - 1].get_frames()) / 75.;
        }

        if(seconds < 0.)
        {
            perror("The audio CD returned a negative total playing time");

            exit(-1);
        }

        hours = int(seconds / 3600.);
        seconds -= ((float) hours * 3600.);
        minutes = int(seconds / 60.);
        seconds -= ((float) minutes * 60.);
    };

    void get_total_time(int& hours, int& minutes, float& seconds)
    {
        seconds = get_total_seconds();

        if(seconds < 0)
        {
            std::cout << "The audio CD returned a negative total playing "
                "time.\n";

            exit(-1);
        }

        hours = int(seconds / 3600.);
        seconds -= (static_cast< float >(hours) * 3600.);
        minutes = int(seconds / 60.);
        seconds -= (static_cast< float >(minutes) * 60.);
    };


    private:
    void open_drive()
    {
        if(drive_file != 0)
            close_drive();

        if(!file.open(QIODevice::ReadOnly))
        {
            const int err(errno);
            std::cout << "Opening the audio CD device failed: "
                << std::strerror(err)
                << "\n";

            exit(err);
        }
        drive_file = file.handle();

        if(drive_file < 0)
        {
            const int err(errno);
            std::cout << "Getting file handle for CD device failed: "
                << std::strerror(err)
                << "\n";
            close_drive();

            exit(err);
        }
    };

    void close_drive()
    {
        file.close();
        drive_file = 0;
    };

    float get_total_seconds()
    {
        return (static_cast< float >(get_frames(get_tracks()) / 75.));
    };

    float get_seconds(unsigned int track)
    {
        return (static_cast< float >(get_frames(track) / 75.));
    };

    int read_toc()
    {
        if(ioctl(drive_file, CDROMREADTOCHDR, &tochdr) != 0)
        {
            const int err(errno);
            std::cout << "Reading of audio CD's table of contents failed: "
                << std::strerror(err)
                << "\n";
            close_drive();

            exit(err);
        }

        for(unsigned int i(tochdr.cdth_trk0); i <= tochdr.cdth_trk1; ++i)
        {
            tocentry.cdte_track = i;
            tocentry.cdte_format = CDROM_MSF;
            if(ioctl(drive_file, CDROMREADTOCENTRY, &tocentry) != 0)
            {
                const int err (errno);
                std::cout << "Reading of audio CD's table of contents "
                    "entry failed: "
                    << std::strerror(err)
                    << "\n";
                close_drive();

                exit(err);
            }
            cdtoc[i - 1].set(tocentry.cdte_addr.msf.minute,
                tocentry.cdte_addr.msf.second, tocentry.cdte_addr.msf.frame);
            cdtoc[i - 1].set_frames(cdtoc[i - 1].get_frames()
                + cdtoc[i - 1].get_min() * 60 * 75);
            cdtoc[i - 1].set_frames(cdtoc[i - 1].get_frames()
                + cdtoc[i - 1].get_sec() * 75);

            if(i >= MAX_TOC_ENTRIES)
            {
                break;
            }
        }

        tocentry.cdte_track = 0xAA;
        tocentry.cdte_format = CDROM_MSF;
        if(ioctl(drive_file, CDROMREADTOCENTRY, &tocentry) != 0)
        {
            const int err(errno);
            std::cout << "Reading of audio CD's total frames failed: "
                << std::strerror(err)
                << "\n";
            close_drive();

            exit(err);
        }

        cdtoc[tochdr.cdth_trk1].set(tocentry.cdte_addr.msf.minute,
            tocentry.cdte_addr.msf.second, tocentry.cdte_addr.msf.frame);
        cdtoc[tochdr.cdth_trk1].set_frames(
            cdtoc[tochdr.cdth_trk1].get_frames()
                + cdtoc[tochdr.cdth_trk1].get_min() * 60 * 75);
        cdtoc[tochdr.cdth_trk1].set_frames(
            cdtoc[tochdr.cdth_trk1].get_frames()
                + cdtoc[tochdr.cdth_trk1].get_sec() * 75);

        close_drive();

        return (tochdr.cdth_trk1);
    };

    unsigned int cddb_sum(int n)
    {
        unsigned int ret(0);

        while(n > 0)
        {
            ret += (n % 10);
            n /= 10;
        };

        return (ret);
    };

    unsigned long cddb_discid()
    {
        unsigned int i(0), t(0), n(0);

        while(i < tracks)
        {
            n += cddb_sum((cdtoc[i].get_min() * 60) + cdtoc[i].get_sec());
            ++i;
        };

        t = ((cdtoc[tracks].get_min() * 60) + cdtoc[tracks].get_sec())
            - ((cdtoc[0].get_min() * 60) + cdtoc[0].get_sec());

        return ((n % 0xff) << 24 | t << 8 | tracks);
    };


    QString drive;
    QFile file;
    int drive_file;
    unsigned int tracks;
    unsigned long discid;
    struct cdrom_tochdr tochdr;
    struct cdrom_tocentry tocentry;
    toc cdtoc[MAX_TOC_ENTRIES];
};
#endif
